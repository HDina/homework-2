#!/usr/bin/env python

from nose.tools import *

from aims import std

def test_ints():
    liste = [2, 2]
    by_this_function = std(liste)
    by_calculus = 0
    assert_equal(by_this_function, by_calculus)

def test_float():
    liste = [2.0, 2.0, 8.0]
    by_this_function = std(liste)
    by_calculus = 2.82843
    assert_equal(by_this_function, by_calculus)

def test_negative():
    liste = [-2.0, -2.0, 6.5]
    by_this_function = std(liste)
    by_calculus = 4.00694
    assert_equal(by_this_function, by_calculus)

def test_false():
    liste = [-2.0, -2.0, 6.5]
    by_this_function = std(liste)
    by_calculus = 4.00694
    assert_false(by_this_function != by_calculus)

def test_true():
    liste = [-2.0, -2.0, 6.5]
    by_this_function = std(liste)
    by_calculus = 4.00694
    assert_true(by_this_function == by_calculus)



