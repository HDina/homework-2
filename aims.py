#!/usr/bin/env python

def main():
    print "Welcome to the AIMS module"

if __name__ == "__main__":
    main()


def std(liste):
    import math
    difference = []
    N = len(liste) 
    try:
        for x in liste:      
            x_average = sum(liste) / float(N)
            y = (x - x_average)**2
            difference.append(y)
        deviation = math.sqrt(sum(difference)/float(N))    
        return round(deviation, 5)
    except ZeroDivisionError as detail:
        msg = "Your list is empty."
        raise ZeroDivisionError(detail.__str__() + "\n" + "\n" + msg)
    


def avg_range(filenames):
    filedata = [open(filename, 'r') for filename in filenames]
    liste = []    
    for one_element in filedata:
        for line in one_element:
            if line.startswith("Range: "):
                range_value = line.strip()
                range_value = range_value.split(" ")
                range_value = int(range_value[1])
                liste.append(range_value)
    return sum(liste)/float(len(liste))
    filedata.close()

    
